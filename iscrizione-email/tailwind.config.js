/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./dist/*.{html,js}', './images/*'],
  theme: {
    extend: {},
  },
  plugins: [],
}
